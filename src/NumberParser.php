<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);

namespace Youngsource\Helpers;

use DomainException;
use NumberFormatter;
use function intl_is_failure;
use function is_string;

/**
 * Class NumberParser
 * Class responsible for parsing numbers in the application.
 *
 * @package App\Algorithms
 * @invar $styleFrom can never equal $styleTo
 */
final class NumberParser extends NumberFormatter
{
    /** @var NumberFormatter */
    private $styleToFormatter;

    /**
     * NumberParser constructor.
     *
     * @param NumberStyleEnum|null $styleFrom
     * @param NumberStyleEnum|null $styleTo
     */
    public function __construct(?NumberStyleEnum $styleFrom = null, ?NumberStyleEnum $styleTo = null)
    {
        $styleTo = $styleTo ?? NumberStyleEnum::BELGIAN();
        $styleFrom = $styleFrom ?? NumberStyleEnum::AMERICAN();
        $this->styleToFormatter = static::create(
            $styleTo->getValue(),
            NumberFormatter::DECIMAL
        );
        parent::__construct($styleFrom->getValue(), NumberFormatter::DECIMAL);
    }

    /**
     * Transforms the typical belgian number input to a float representation.
     *
     * @param string $number
     * @return float
     * @throws DomainException
     */
    public function toFloat(string $number): float
    {
        return (float) $this->strictParse($this, $number);
    }

    /**
     * @param NumberFormatter $formatter
     * @param string $number
     * @return float|int
     */
    private function strictParse(NumberFormatter $formatter, string $number)
    {
        /** @var float|int $parse */
        $parse = $formatter->parse($number);
        $this->checkForFailure($formatter, $number);
        return $parse;
    }

    /**
     * Defensively checks if the formatting process was legal.
     *
     * @param NumberFormatter $formatter
     * @param mixed $number
     * @throws DomainException
     */
    private function checkForFailure(NumberFormatter $formatter, $number): void
    {
        if (intl_is_failure($formatter->getErrorCode())) {
            $locale = $this->getLocale();
            throw new DomainException("The number: '$number' could not be parsed with the current locale: $locale.");
        }
    }

    /**
     * Transforms the number input to an integer representation.
     *
     * @param string $number
     * @return int
     * @throws DomainException
     */
    public function toInt(string $number): int
    {
        return (int) $this->strictParse($this, $number);
    }

    /**
     * Displays the given number as a percentage.
     *
     * @param string|int|float $number the number to display as percentage
     * @param int $precision the precision of the number to parse
     * @return      string  the parsed number with the given parameter with a percentage sign concatenated to it
     * @throws DomainException
     */
    public function displayAsPercentage($number, int $precision = 2): string
    {
        return $this->parseNumber($number, $precision) . '%';
    }

    /**
     * Parse a number and display it with a given precision.
     *
     * @param   string|int|float $number the number that needs to be parsed
     * @param   int $precision the precision of the number to parse
     * @return  string the number conforming to the standard represented by $styleTo
     * @throws DomainException
     */
    public function parseNumber($number, int $precision = 2): string
    {
        $number = ($number === -0.0) ? 0.0 : $number;
        $this->styleToFormatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, $precision);
        $this->styleToFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, $precision);
        if (is_string($number)) {
            $tbr = $this->strictFormat($this->styleToFormatter, $this->strictParse($this, $number));
        } else {
            $tbr = $this->strictFormat($this->styleToFormatter, $number);
        }
        return $tbr;
    }

    /**
     * @param NumberFormatter $formatter
     * @param float $number
     * @return string
     * @throws DomainException
     */
    private function strictFormat(NumberFormatter $formatter, float $number): string
    {
        $tbr = $formatter->format($number);
        $this->checkForFailure($formatter, $number);
        return $tbr;
    }

    /**
     * Displays the given number as a euro.
     *
     * @param string|int|float $number the number to display as percentage
     * @param int $precision the precision of the number to parse
     * @return      string  the parsed number with the given parameter with a euro symbol pushed in front of it.
     * @throws DomainException
     */
    public function displayAsEuro($number, int $precision = 2): string
    {
        return '&euro; ' . $this->parseNumber($number, $precision);
    }

    /**
     * Displays the given number as a dollars.
     *
     * @param string|int|float $number the number to display as percentage
     * @param int $precision the precision of the number to parse
     * @return      string  the parsed number with the given parameter with a euro symbol pushed in front of it.
     * @throws DomainException
     */
    public function displayAsDollar($number, int $precision = 2): string
    {
        return '$ ' . $this->parseNumber($number, $precision);
    }
}
