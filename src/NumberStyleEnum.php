<?php /** @noinspection PhpUnused */
/** @noinspection LowerAccessLevelInspection */
declare(strict_types=1);

namespace Youngsource\Helpers;

use Youngsource\TypedEnum\TypedEnum;

/**
 * @class   NumberStyle
 * An enumeration that enumerates all the styles of number display Parser supports
 * @method static NumberStyleEnum AMERICAN()
 * @method static NumberStyleEnum BELGIAN()
 */
final class NumberStyleEnum extends TypedEnum
{
    protected const AMERICAN = 'en_US';
    protected const BELGIAN = 'nl_BE';
}
