<?php
declare(strict_types=1);

if (!function_exists('swap')) {
    /**
     * Swap both variables.
     *
     * @param mixed $left
     * @param mixed $right
     */
    function swap(&$left, &$right): void
    {
        $tmp = $left;
        $left = $right;
        $right = $tmp;
    }
}

if (!function_exists('clone_')) {
    /**
     * Clones the given value.
     *
     * @param mixed $var
     * @return mixed
     */
    function clone_($var)
    {
        return is_object($var) ? clone $var : $var;
    }
}
