<?php
declare(strict_types=1);

if (!function_exists('microtime_float')) {
    /**
     * Returns the time in microseconds.
     *
     * @return float
     */
    function microtime_float(): float
    {
        [$usec, $sec] = explode(' ', microtime());
        return (float)$usec + (float)$sec;
    }
}

if (!function_exists('key_range')) {
    /**
     * Extended range function where the values will be equal to the keys.
     *
     * @param int|float $start
     * @param int|float $end
     * @param int|float $step
     * @return array
     */
    function key_range($start, $end, $step = 1): array
    {
        $tbr = [];
        foreach (range($start, $end, $step) as $value) {
            $tbr[(string)$value] = $value;
        }
        return $tbr;
    }
}
