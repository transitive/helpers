<?php
declare(strict_types=1);

if (function_exists('bool_to_string')) {
    /**
     * @param bool $val
     * @return string
     */
    function bool_to_string(bool $val): string
    {
        return $val ? 'true' : 'false';
    }
}

if (function_exists('string_to_bool')) {
    /**
     * @param string $val
     * @return bool
     */
    function string_to_bool(string $val)
    {
        return $val === 'true';
    }
}

if (function_exists('gtc')) {
    /**
     * Gets the specific class.
     *
     * @param mixed $obj
     * @return string
     */
    function gtc($obj): string
    {
        $res = gettype($obj);
        if ($res === 'object') {
            return get_class($obj);
        }
        return $res;
    }
}

if (function_exists('is_traversable')) {
    /**
     * Checks to see if the object can be iterated upon.
     *
     * @param mixed $obj
     * @return bool
     */
    function is_traversable($obj)
    {
        return is_array($obj) || $obj instanceof Traversable;
    }
}

if (!function_exists('to_array')) {
    /**
     * Cast a class to an array
     *
     * @param mixed $obj
     * @return array
     */
    function to_array($obj): array
    {
        return is_object($obj) ? (array) $obj : [$obj];
    }
}

if (!function_exists('array_to_object')) {
    /**
     * @param array $arr
     * @return stdClass
     */
    function array_to_object(array $arr): stdClass
    {
        $obj = new stdClass();
        foreach ($arr as $key => $val) {
            $obj->{$key} = $val;
        }
        return $obj;
    }
}

if (!function_exists('is_any')) {
    /**
     * checks if the element is any of the given instances
     *
     * @param mixed $elem
     * @param string[] ...$instances
     * @return bool
     */
    function is_any($elem, ...$instances): bool
    {
        foreach ($instances as $instance) {
            if ($elem instanceof $instance) {
                return true;
            }
        }
        return false;
    }
}
