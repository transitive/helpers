<?php
declare(strict_types=1);

namespace App\Algorithms\Extraction;

use Exception;
use function array_keys;
use function array_slice;
use function count;
use function function_exists;
use function random_int;

if (!function_exists('tail')) {
    /**
     * Returns the tail of the array.
     *
     * @param array $arr
     * @return array
     */
    function tail(array $arr)
    {
        return array_slice($arr, 1);
    }
}

if (!function_exists('pick_any')) {
    /**
     * Picks a random item from the array.
     *
     * @param array $array
     * @return mixed
     * @throws Exception
     */
    function pick_any(array $array)
    {
        $keys = array_keys($array);
        return $array[$keys[random_int(0, count($keys) - 1)]];
    }
}
