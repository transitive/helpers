<?php
declare(strict_types=1);

if (!function_exists('is_between')) {
    /**
     * checks to see if the number is between or equal to the given bounds of the form [[a,b], [c,d], ...]
     *
     * @param int|float $num the number the check if it is between the given sets of bounds
     * @param array ...$bounds the sets of bounds
     * @return bool true if it is between the given sets of bounds
     */
    function is_between($num, array $bounds): bool
    {
        for ($i = 0, $iMax = count($bounds); $i < $iMax; $i += 2) {
            if ($bounds[$i] > $num || $bounds[$i + 1] < $num) {
                return false;
            }
        }
        return true;
    }
}

if (!function_exists('numeric_order')) {
    /**
     * Returns the numerical order between the two numbers
     *
     * @param int|float $lhs
     * @param int|float $rhs
     * @return int
     */
    function numeric_order($lhs, $rhs): int
    {
        if ($lhs === $rhs) {
            return 0;
        }
        return ($lhs < $rhs) ? -1 : 1;
    }
}
