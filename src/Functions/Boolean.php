<?php
declare(strict_types=1);

if (!function_exists('sc_or')) {
    /**
     * short circuit or operation
     *
     * @param array ...$values
     * @return mixed|null
     */
    function sc_or(...$values)
    {
        foreach ($values as $value) {
            if ($value !== null) {
                return $value;
            }
        }
        return null;
    }
}

if (!function_exists('sc_and')) {
    /**
     * short circuit and operation
     *
     * @param array ...$values
     * @return bool|mixed
     */
    function sc_and(...$values)
    {
        foreach ($values as $val) {
            /** @noinspection TypeUnsafeComparisonInspection Checks for false'ish values */
            if ($val == 0) {
                return $val;
            }
        }

        return true;
    }
}
