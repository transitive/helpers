<?php
/** @noinspection UnqualifiedReferenceInspection */
declare(strict_types=1);

use Youngsource\Helpers\NumberParser;

if (!function_exists('str_split')) {
    /**
     * Splits a string into an array
     *
     * @param string $string the string to split
     * @param int $length amount of characters between split
     * @return array
     */
    function str_split(string $string, int $length = 0): array
    {
        if ($length > 0) {
            $ret = [];
            $len = mb_strlen($string, 'UTF-8');

            for ($i = 0; $i < $len; $i += $length) {
                $ret[] = mb_substr($string, $i, $length, 'UTF-8');
            }

            return $ret;
        }

        $result = preg_split('//u', $string, -1, PREG_SPLIT_NO_EMPTY);
        return $result === false ? [] : $result;
    }
}

if (!function_exists('d_euro')) {
    /**
     * Display the item as a euro
     *
     * @param mixed $val
     * @param int $precision
     * @return string
     */
    function d_euro($val, int $precision = 2): string
    {
        return (new NumberParser)->displayAsEuro($val, $precision);
    }
}

if (!function_exists('d_percentage')) {
    /**
     * @param mixed $val
     * @param int $precision
     * @return string
     */
    function d_percentage($val, int $precision = 2): string
    {
        return (new NumberParser)->displayAsPercentage($val, $precision);
    }
}

if (!function_exists('d_dollar')) {
    /**
     * @param mixed $val
     * @param int $precision
     * @return string
     */
    function d_dollar($val, int $precision = 2): string
    {
        return (new NumberParser)->displayAsPercentage($val, $precision);
    }
}

if (!function_exists('d_number')) {
    /**
     * @param mixed $val
     * @param int $precision
     * @return string
     */
    function d_number($val, int $precision = 2): string
    {
        return (new NumberParser)->parseNumber($val, $precision);
    }
}

if (!function_exists('round_on_words')) {
    /**
     * Round the text with a maximal number of chars respecting full words.
     *
     * @param string $word
     * @param int $characters
     * @param string $end
     * @return string
     */
    function round_on_words(string $word, int $characters, string $end = ' ...'): string
    {
        $limit = $characters - strlen($end); // Take into account $end string into the limit
        $wordLength = strlen($word);
        $length = strrpos(substr($word, 0, $limit), ' ');
        $length = $length === false ? $limit : $length;
        return $limit < $wordLength ? substr($word, 0, $length) . $end : $word;
    }
}

if (!function_exists('d_bool_to_string')) {
    /**
     * @param bool $val
     * @return string
     */
    function d_bool_to_string(bool $val): string
    {
        if ($val) {
            return 'WAAR';
        }

        return 'ONWAAR';
    }
}
