<?php
declare(strict_types=1);

namespace Youngsource\Helpers;

use function function_exists;

/**
 * Simple object wrapper to act a pointer.
 *
 * @package App\CustomTypes
 */
class Pointer
{
    /** @var mixed */
    public $value;

    /**
     * Ptr constructor.
     *
     * @param mixed $value
     */
    public function __construct(&$value)
    {
        $this->value = $value;
    }
}

if (!function_exists('ptr')) {
    /**
     * Creates a pointer around this object.
     *
     * @param mixed $obj
     * @return Pointer
     */
    function ptr($obj): Pointer
    {
        return new Pointer($obj);
    }
}
