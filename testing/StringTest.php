<?php

namespace Youngsource\Helpers\Testing;

use PHPUnit\Framework\TestCase;
use function round_on_words;

class StringTest extends TestCase
{
    public function testString(): void
    {
        self::assertEquals('test me dan in hemelsnaam ...', round_on_words('test me dan in hemelsnaam potjandorie', 30));
    }
    public function testStringLess(): void
    {
        self::assertEquals('BTW club', round_on_words('BTW club', 50));
    }
}
