<?php
declare(strict_types=1);

namespace Youngsource\Helpers\Testing;

use PHPUnit\Framework\TestCase;
use Youngsource\Helpers\NumberParser;

/**
 * Class NumberParserTest
 * @package Tests\Unit\Algorithms
 */
final class NumberParserTest extends TestCase
{
    /** @var NumberParser */
    private $parser;

    public function __construct()
    {
        parent::__construct();
        $this->setParser(new NumberParser);
    }

    public function testParseNumber(): void
    {
        $this->assertEquals('10.000,25', $this->getParser()->parseNumber('10000.25', 2));
    }

    public function testToFloat(): void
    {
        $this->assertEquals(10000.25, $this->getParser()->toFloat('10000.25'));
    }

    public function testToInt(): void
    {
        $this->assertEquals(10000, $this->getParser()->toInt('10000.25'));
    }

    public function testDisplayAsPercentage(): void
    {
        $this->assertEquals('100,00%', $this->getParser()->displayAsPercentage(100, 2));
    }

    public function testDisplayAsEuro(): void
    {
        $this->assertEquals('&euro; 10.000,25', $this->getParser()->displayAsEuro('10000.25', 2));
    }

    public function testDisplayAsDollar(): void
    {
        $this->assertEquals('$ 0,00', $this->getParser()->displayAsDollar('0', 2));
    }

    /**
     * @return NumberParser
     */
    public function getParser(): NumberParser
    {
        return $this->parser;
    }

    /**
     * @param NumberParser $parser
     */
    public function setParser(NumberParser $parser): void
    {
        $this->parser = $parser;
    }
}
